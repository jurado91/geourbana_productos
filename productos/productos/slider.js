jQuery(document).ready(function ($) {
  var move = moveRight
  var intervalo = setInterval(function () {
    move()
  }, 3000)

  
  var slideCount = $('.slider_well.slider ul li').length
  var slideWidth = $('.slider_well.slider ul li').width()
  var slideHeight = $('.slider_well.slider ul li').height()
  var sliderUlWidth = slideCount * slideWidth
  
  $('.slider_well.slider').css({ width: slideWidth, height: slideHeight })
  
  $('.slider_well.slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth })
  
  $('.slider_well.slider ul li:last-child').prependTo('.slider_well.slider ul')

  function moveLeft() {
    $('.slider_well.slider ul').animate({
      left: + slideWidth
    }, 200, function () {
      $('.slider_well.slider ul li:last-child').prependTo('.slider_well.slider ul')
      $('.slider_well.slider ul').css('left', '')
    })
  }

  function moveRight() {
    $('.slider_well.slider ul').animate({
      left: - slideWidth
    }, 400, function () {
      $('.slider_well.slider ul li:first-child').appendTo('.slider_well.slider ul')
      $('.slider_well.slider ul').css('left', '')
    })
  }

  $('a.control_prev').click(function () {
    moveLeft()
    move = moveLeft
  })

  $('a.control_next').click(function () {
    moveRight()
    move = moveRight
  })
  window.addEventListener("resize", function() {
    location.reload()
  })
})