$(document).ready(function (){

	var bottomRequired = null;
	var bottomActual = null;
	var bottomDiff = null;

	$(window).load(function (){
		
		
		bottomRequired = $('#panel_servicios').offset().top + $('#panel_servicios').outerHeight();
		bottomActual = $('#ultimo_panel_inferior').offset().top + $('#ultimo_panel_inferior').outerHeight();
		bottomDiff =  Math.round(bottomRequired - bottomActual) + 0.5;

		$('#ultimo_panel_inferior').css('height',($('#ultimo_panel_inferior').outerHeight()+bottomDiff)+'px');


		var container_width = $('#facebook').width();    
	    $('#facebook').html(' <div class="fb-page" data-width="'+container_width+'" data-href="https://www.facebook.com/pages/Geograf%C3%ADa-Urbana/177733859297?fref=ts" data-tabs="timeline" data-small-header="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pages/Geograf%C3%ADa-Urbana/177733859297?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pages/Geograf%C3%ADa-Urbana/177733859297?fref=ts">Geografía Urbana</a></blockquote></div>');
  		
	});

	$(window).resize(function (){
		if($(window).width() >= 992){
			bottomRequired = $('#panel_servicios').offset().top + $('#panel_servicios').outerHeight();
			bottomActual = $('#ultimo_panel_inferior').offset().top + $('#ultimo_panel_inferior').outerHeight();
			bottomDiff =  Math.round(bottomRequired - bottomActual) + 0.5;

			$('#ultimo_panel_inferior').css('height',($('#ultimo_panel_inferior').outerHeight()+bottomDiff)+'px');
		}
	});

	$('#panel_redes > div.row:nth-child(2)').on('mouseover', function (){
		$(this).css('overflow','auto');
	});
			
	$('#panel_redes > div.row:nth-child(2)').on('mouseout', function (){
		$(this).css('overflow','hidden');	
	});
	
	$('#panel_redes > div.row:nth-child(2)').css('height',($('#panel_redes_padre').height()-$('#panel_redes > div.row:nth-child(1)').height())+'px');

	$(window).resize(function (){
		$('#panel_redes > div.row:nth-child(2)').css('height',($('#panel_redes_padre').height()-$('#panel_redes > div.row:nth-child(1)').height())+'px');
	});	

	$('#boton_twitter a').on('click',function (){
		$('#boton_facebook').removeClass('boton_facebook');
		$('#boton_twitter').addClass('boton_twitter');
	});
	
	$('#boton_facebook a').on('click',function (){
		$('#boton_facebook').addClass('boton_facebook');
		$('#boton_twitter').removeClass('boton_twitter');
	});
	
	$('#icono1').on('click',function (){
		$('#icono1').addClass('icono1');
		$('#icono2').removeClass('icono2');
		$('#icono3').removeClass('icono3');
		$('#icono4').removeClass('icono4');
		$('#icono5').removeClass('icono5');
	});
	
	$('#icono2').on('click',function (){
		$('#icono1').removeClass('icono1');
		$('#icono2').addClass('icono2');
		$('#icono3').removeClass('icono3');
		$('#icono4').removeClass('icono4');
		$('#icono5').removeClass('icono5');
	});
	
	$('#icono3').on('click',function (){
		$('#icono1').removeClass('icono1');
		$('#icono2').removeClass('icono2');
		$('#icono3').addClass('icono3');
		$('#icono4').removeClass('icono4');
		$('#icono5').removeClass('icono5');
	});
	
	$('#icono4').on('click',function (){
		$('#icono1').removeClass('icono1');
		$('#icono2').removeClass('icono2');
		$('#icono3').removeClass('icono3');
		$('#icono4').addClass('icono4');
		$('#icono5').removeClass('icono5');
	});
	
	$('#icono5').on('click',function (){
		$('#icono1').removeClass('icono1');
		$('#icono2').removeClass('icono2');
		$('#icono3').removeClass('icono3');
		$('#icono4').removeClass('icono4');
		$('#icono5').addClass('icono5');
	});
});